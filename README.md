# simplekernel-rs

Very simple ```"Hello, World!"``` i386 freestanding Rust.

## Pre built
- nightly toolchain for cross compilation.
- rust-src for core+compiler-rt

```sh

rustup toolchain add nightly
rustup component add rust-src --toolchain nightly

```

## How to build simplekernel-rs

```sh

git clone https://github.com/hwoy/simplekernel-rs.git
cd simplekernel-rs
cargo build -r

```
## How to run simplekernel-rs
- multiboot only not multiboot2

```sh

qemu-system-i386 -kernel simplekernel-rs

```

## How to build bootable CD
- multiboot/multiboot2 can be booted on grub.

```sh
# build with multiboot2
cargo build -r -F multiboot2_header

# make cdboot code
grub-mkimage -o grub.img --format=i386-pc-eltorito --prefix="(cd)/boot/grub" multiboot multiboot2 iso9660 biosdisk

ISO_DIR=iso
BOOT_FILE=grub.img
OUTPUT=cdrom.iso

# make cd iso
xorrisofs -r -J -o ${OUTPUT} -b ${BOOT_FILE} -no-emul-boot -boot-info-table  -boot-load-size 4 ${ISO_DIR}

# boot with qemu
qemu-system-i386 -cdrom ${OUTPUT}


```

```sh
tree iso
iso
├── boot
│   ├── grub
│   │   └── grub.cfg
│   └── simplekernel-rs
└── grub.img

3 directories, 3 files

cat grub.cfg

set timeout=15
set default=0 # Set the default menu entry

menuentry "Hwoy OS" {
    multiboot2 (cd)/boot/simplekernel-rs
    boot
}

```

## Contact me

- Web: <https://github.com/hwoy>
- Email: <mailto:bosskillerz@gmail.com>
- Facebook: <https://www.facebook.com/watt.duean>
