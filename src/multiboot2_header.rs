/*multiboot2.rs*/

#[repr(C, align(8))]
struct MultiBoot2 {
    magic: u32,
    arch: u32,
    length: u32,
    checksum: u32,
    _type: u16,
    _flags: u16,
    _size: u32,
}

const MAGIC: u32 = 0xe85250d6;
const ARCH: u32 = 0;
const LENGTH: u32 = core::mem::size_of::<MultiBoot2>() as u32;

impl MultiBoot2 {
    const fn check_sum(magic: u32, arch: u32, length: u32) -> u32 {
        !(magic + arch + length) + 1
    }

    const fn new() -> Self {
        Self {
            magic: MAGIC,
            arch: ARCH,
            length: LENGTH,
            checksum: Self::check_sum(MAGIC, ARCH, LENGTH),
            _type: 0,
            _flags: 0,
            _size: 8,
        }
    }
}

#[used]
#[no_mangle]
#[link_section = ".multiboot2"]
static multiboot2: MultiBoot2 = MultiBoot2::new();
