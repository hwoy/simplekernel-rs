/*multiboot2_header.rs*/

macro_rules! bytes_multiboot2 {
    () => {
        include_bytes!("../multiboot2/multiboot2_header.bin")
    };
}

#[used]
#[no_mangle]
#[link_section = ".multiboot2"]
static multiboot2: [u8; bytes_multiboot2!().len()] = *bytes_multiboot2!();
