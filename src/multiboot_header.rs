/*multiboot.rs*/

#[repr(C, align(4))]
struct MultiBoot {
    magic: u32,
    flags: u32,
    checksum: u32,
}

const ALIGN: u32 = 1 << 0;
const MEMINFO: u32 = 1 << 1;
const MAGIC: u32 = 0x1BADB002;
const FLAGS: u32 = ALIGN | MEMINFO;

impl MultiBoot {
    const fn check_sum(magic: u32, flags: u32) -> u32 {
        !(magic + flags) + 1
    }

    const fn new() -> Self {
        Self {
            magic: MAGIC,
            flags: FLAGS,
            checksum: Self::check_sum(MAGIC, FLAGS),
        }
    }
}

#[used]
#[no_mangle]
#[link_section = ".multiboot"]
static multiboot: MultiBoot = MultiBoot::new();
