/*panic.rs*/

use core::panic::PanicInfo;
use vgastream_rs::prelude::*;

use core::arch::asm;
#[inline]
pub unsafe fn halt() -> ! {
    asm!("cli", "hlt");
    core::hint::unreachable_unchecked()
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    eprintln!("{}", info);
    loop {
        unsafe { halt() }
    }
}
