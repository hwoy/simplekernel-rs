/*main.rs*/

#![no_std]
#![no_main]

#[cfg(all(feature = "multiboot2_header", feature = "multiboot2_header_bin"))]
compile_error!(r##"You can't use both "multiboot2_header" and "multiboot2_header_bin" feature"##);

#[cfg(not(feature = "multiboot2_header"))]
#[cfg(not(feature = "multiboot2_header_bin"))]
mod multiboot_header;

#[cfg(feature = "multiboot2_header")]
mod multiboot2_header;

#[cfg(feature = "multiboot2_header_bin")]
mod multiboot2_header_bin;

mod panic;

extern crate vgastream_rs;
use vgastream_rs::prelude::*;

use vga_rs::Color;

const DEFAULT_COLORS: (Color, Color) = (Color::BLACK, Color::WHITE);

#[no_mangle]
pub extern "C" fn _start() -> ! {
    vga_screen::reset_with(DEFAULT_COLORS);

    let _ = main();
    loop {
        unsafe { panic::halt() }
    }
}

fn main() {
    println!("println!: Hello, World!");

    let mut streamcolor = VgaOutStreamColor::new((Color::RED, Color::WHITE), DEFAULT_COLORS);
    streamcolor.print_fmt(format_args!("streamcolor: Hello, World!\n"));

    streamcolor.set_color((Color::RED, Color::BLUE));
    streamcolor.print_fmt(format_args!("streamcolor: Hello, World!\n"));

    streamcolor.set_color((Color::BLUE, Color::RED));
    streamcolor.print_fmt(format_args!(
        "streamcolor: Hello, World!\nstreamcolor: Hello, World!\n"
    ));

    println!("println!: Hello, World!");

    let mut stream = VgaOutStream::new();
    stream.print_fmt(format_args!("streamcolor: Hello, World!\n"));

    streamcolor.set_color((Color::GREEN, Color::WHITE));

    for _ in 0..5 {
        for i in 0..10 {
            streamcolor.print_fmt(format_args!("{} ", i * i));
        }
        streamcolor.print_fmt(format_args!("\n"));
    }
}
