use multiboot2_header::builder::HeaderBuilder;
use multiboot2_header::HeaderTagISA;

use std::fs::OpenOptions;
use std::io::Write;

/// Small example that creates a Multiboot2 header and parses it afterwards.
fn main() {
    // We create a Multiboot2 header during runtime here. A practical example is that your
    // program gets the header from a file and parses it afterwards.
    let mb2_hdr_bytes = HeaderBuilder::new(HeaderTagISA::I386).build();

    let _ = std::fs::create_dir("multiboot2");

    let mut f = OpenOptions::new()
        .truncate(true)
        .write(true)
        .create(true)
        .open("multiboot2/multiboot2_header.bin")
        .unwrap();
    f.write(mb2_hdr_bytes.as_bytes()).unwrap();
}
